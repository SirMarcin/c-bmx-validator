﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMX_Validator
{
    public static class Props
    {
        public static List<string> EXTENSION_LIST;
        
        public static bool IsInExtensionList(string extension)
        {
            int size = EXTENSION_LIST.Count;
            bool isIn = false;

            for (int i = 0; i < size; i++)
            {
                if (EXTENSION_LIST.ElementAt<string>(i).Equals(extension))
                {
                    isIn = true;
                    break;
                }
            }

            return isIn;
        }
    }
}
