﻿namespace BMX_Validator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.buttonValidate = new System.Windows.Forms.Button();
            this.textBoxMessages = new System.Windows.Forms.TextBox();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.checkBoxDTD = new System.Windows.Forms.CheckBox();
            this.checkBoxXSD = new System.Windows.Forms.CheckBox();
            this.groupBoxValType = new System.Windows.Forms.GroupBox();
            this.groupBoxSettings.SuspendLayout();
            this.groupBoxValType.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(13, 13);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(867, 293);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // buttonValidate
            // 
            this.buttonValidate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonValidate.Location = new System.Drawing.Point(886, 13);
            this.buttonValidate.Name = "buttonValidate";
            this.buttonValidate.Size = new System.Drawing.Size(62, 293);
            this.buttonValidate.TabIndex = 1;
            this.buttonValidate.Text = "Validate";
            this.buttonValidate.UseVisualStyleBackColor = true;
            // 
            // textBoxMessages
            // 
            this.textBoxMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMessages.Location = new System.Drawing.Point(13, 313);
            this.textBoxMessages.Multiline = true;
            this.textBoxMessages.Name = "textBoxMessages";
            this.textBoxMessages.Size = new System.Drawing.Size(457, 264);
            this.textBoxMessages.TabIndex = 2;
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.Controls.Add(this.groupBoxValType);
            this.groupBoxSettings.Location = new System.Drawing.Point(477, 313);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Size = new System.Drawing.Size(471, 264);
            this.groupBoxSettings.TabIndex = 3;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = "Settings";
            // 
            // checkBoxDTD
            // 
            this.checkBoxDTD.AutoSize = true;
            this.checkBoxDTD.Location = new System.Drawing.Point(6, 19);
            this.checkBoxDTD.Name = "checkBoxDTD";
            this.checkBoxDTD.Size = new System.Drawing.Size(49, 17);
            this.checkBoxDTD.TabIndex = 0;
            this.checkBoxDTD.Text = "DTD";
            this.checkBoxDTD.UseVisualStyleBackColor = true;
            // 
            // checkBoxXSD
            // 
            this.checkBoxXSD.AutoSize = true;
            this.checkBoxXSD.Location = new System.Drawing.Point(6, 43);
            this.checkBoxXSD.Name = "checkBoxXSD";
            this.checkBoxXSD.Size = new System.Drawing.Size(48, 17);
            this.checkBoxXSD.TabIndex = 1;
            this.checkBoxXSD.Text = "XSD";
            this.checkBoxXSD.UseVisualStyleBackColor = true;
            // 
            // groupBoxValType
            // 
            this.groupBoxValType.Controls.Add(this.checkBoxDTD);
            this.groupBoxValType.Controls.Add(this.checkBoxXSD);
            this.groupBoxValType.Location = new System.Drawing.Point(6, 19);
            this.groupBoxValType.Name = "groupBoxValType";
            this.groupBoxValType.Size = new System.Drawing.Size(200, 100);
            this.groupBoxValType.TabIndex = 2;
            this.groupBoxValType.TabStop = false;
            this.groupBoxValType.Text = "Validation type";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(960, 589);
            this.Controls.Add(this.groupBoxSettings);
            this.Controls.Add(this.textBoxMessages);
            this.Controls.Add(this.buttonValidate);
            this.Controls.Add(this.listView1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBoxSettings.ResumeLayout(false);
            this.groupBoxValType.ResumeLayout(false);
            this.groupBoxValType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button buttonValidate;
        private System.Windows.Forms.TextBox textBoxMessages;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.GroupBox groupBoxValType;
        private System.Windows.Forms.CheckBox checkBoxDTD;
        private System.Windows.Forms.CheckBox checkBoxXSD;
    }
}

