﻿using System;
using System.Collections.Generic;

namespace BMX_Validator
{
    public class FileDetails
    {
        public int id { get; private set; }
        public string name { get; private set; }
        public string path { get; private set;  }
        public string state { get; private set; }
        public string msg { get; set; }
        public List<string> errorList { get; private set; }
        
        public static List<FileDetails> allFiles;
        public static int latestID;

        public FileDetails(string p)
        {
            if (!string.IsNullOrEmpty(p))
            {
                this.path = p;
            }
            else
            {
                throw new System.Exception("Cannot create FileDetails object: file path is empty");
            }

            this.name = System.IO.Path.GetFileName(p);
            this.state = String.Empty;
            this.msg = String.Empty;
            this.errorList = new List<string>();
            this.id = ++latestID;

            Console.WriteLine(this.id);
        }

        public FileDetails(string p, string s, string m, List<string> e)
        {
            this.name = System.IO.Path.GetFileName(p);
            this.state = s;
            this.msg = m;
            this.errorList = e;
            this.id = ++latestID;
            Console.WriteLine(this.id);
        }

        public static void ListAll()
        {
            foreach (FileDetails item in FileDetails.allFiles)
            {
                Console.WriteLine(item.id);
            }
        }
    }
}