﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace BMX_Validator
{
    class Validator
    {
        private ListView listView;
        private int currentItemIndex;
        
        public enum ValTypes
        {
            DTD,
            XSD,
            none
        }

        public Validator(ListView listView)
        {
            this.listView = listView;
        }

        public void Validate(Validator.ValTypes valType)
        {
            
            int fileListSize = listView.Items.Count;

            for (currentItemIndex = 0; currentItemIndex < fileListSize; currentItemIndex++)
            {
                // get the path for current XML that should be validated
                string xmlPath = listView.Items[currentItemIndex].SubItems[Form1.index_fileName].Text;

                // set up the XML reader settings
                XmlReaderSettings config = new XmlReaderSettings();

                switch (valType)
                {
                    case ValTypes.DTD:
                        config.ValidationType = ValidationType.DTD;
                        break;
                    case ValTypes.XSD:
                        config.ValidationType = ValidationType.Schema;
                        break;
                    case ValTypes.none:
                        config.ValidationType = ValidationType.None;
                        break;
                    default:
                        config.ValidationType = ValidationType.None;
                        break;
                }

                config.ValidationType = ValidationType.DTD;
                config.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                config.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
                config.ValidationFlags |= XmlSchemaValidationFlags.ProcessSchemaLocation;
                config.DtdProcessing |= DtdProcessing.Parse;
                config.ValidationEventHandler += Config_ValidationEventHandler;
                
                Console.WriteLine("[{1}] Validating {0}", xmlPath, currentItemIndex.ToString());

                // setup the XML reader itself
                XmlReader xml = XmlReader.Create(xmlPath, config);
                
                try
                {
                    while (!xml.EOF)
                    {
                        // go through each XML node
                        xml.Read();

                        switch (xml.NodeType)
                        {
                            case XmlNodeType.None:
                                break;
                            case XmlNodeType.Element:
                                break;
                            case XmlNodeType.Attribute:
                                break;
                            case XmlNodeType.Text:
                                break;
                            case XmlNodeType.CDATA:
                                break;
                            case XmlNodeType.EntityReference:
                                xml.ResolveEntity();
                                break;
                            case XmlNodeType.Entity:
                                break;
                            case XmlNodeType.ProcessingInstruction:
                                break;
                            case XmlNodeType.Comment:
                                break;
                            case XmlNodeType.Document:
                                break;
                            case XmlNodeType.DocumentType:
                                break;
                            case XmlNodeType.DocumentFragment:
                                break;
                            case XmlNodeType.Notation:
                                break;
                            case XmlNodeType.Whitespace:
                                break;
                            case XmlNodeType.SignificantWhitespace:
                                break;
                            case XmlNodeType.EndElement:
                                break;
                            case XmlNodeType.EndEntity:
                                break;
                            case XmlNodeType.XmlDeclaration:
                                break;
                            default:
                                break;
                        }
                    };
                }
                catch (Exception e)
                {
                    // and finally report if there's something wrong
                    Console.WriteLine("!!! " + e.Message);
                    FileDetails.allFiles[currentItemIndex].errorList.Add(e.Message);
                }
            }
        }

        private void Config_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Warning)
            {
                Console.WriteLine("[{1}]\tWarning: Matching schema not found.  No validation occurred. {0}", e.Message, currentItemIndex.ToString());
                
                FileDetails.allFiles[currentItemIndex].errorList.Add(e.Message);
            }
            else
            {
                Console.WriteLine("\tValidation error: {0}", e.Message);
                FileDetails.allFiles[currentItemIndex].errorList.Add(e.Message);
            }
                

        }
        
    }
}
