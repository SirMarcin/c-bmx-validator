﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BMX_Validator
{
    public partial class Form1 : Form
    {
        Validator validator;
        public static int index_fileName, index_status, index_id;

        public Form1()
        {
            InitializeComponent();
            MyInit();
            validator = new Validator(listView1);
        }

        /// <summary>
        /// Initialization of Form1 components outside of original init method
        /// because it's automatically regenerated when a change in WYSIWYG is made
        /// </summary>
        private void MyInit()
        {
            #region listView1 initialization
            
            // COLUMN INDEXES
            index_id = 0;
            index_fileName = 1;
            index_status = 2;

            // COLUMN PROPS
            listView1.Columns.Add("ID", 0);
            listView1.Columns.Add("File name", 500);
            listView1.Columns.Add("Valid", 300);

            // DRAG AND DROP
            listView1.AllowDrop = true;
            listView1.DragDrop += ListView1_DragDrop;
            listView1.DragEnter += ListView1_DragEnter;

            // MISC. PROPS
            listView1.FullRowSelect = true;

            // CONTEXT MENU
            ContextMenuStrip cMenu = new ContextMenuStrip();
            ToolStripMenuItem cRemove = new ToolStripMenuItem("Remove");

            cRemove.Click += CRemove_Click;

            cMenu.Items.AddRange(new ToolStripItem[]
            {
                cRemove
            });

            listView1.ContextMenuStrip = cMenu;

            // SELECTION CHANGE
            listView1.ItemSelectionChanged += ListView1_ItemSelectionChanged;


            #endregion

            #region buttonValidate initialization

            buttonValidate.Click += ButtonValidate_Click;

            #endregion
        }

        private void ListView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            textBoxMessages.Text = string.Empty;
            if (listView1.SelectedItems.Count == 1)
            {
                string finalMessage = string.Empty;
                int selectedItemIndex = listView1.Items.IndexOf(listView1.SelectedItems[0]);

                FileDetails currentItem = FileDetails.allFiles[selectedItemIndex];

                foreach (string message in currentItem.errorList)
                {
                    finalMessage += (message + "\r\n");
                }

                textBoxMessages.AppendText(finalMessage);
            }
        }

        // >> EVENTS

        private void CRemove_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView1.SelectedItems)
            {
                RemoveFile(Int32.Parse(item.SubItems[index_id].Text));
                Console.WriteLine("Removing {0}", item.SubItems[index_fileName].Text);
            }

            FileDetails.ListAll();
        }

        private void ButtonValidate_Click(object sender, EventArgs e)
        {
            bool valTypeDTD = checkBoxDTD.Enabled;
            bool valTypeXSD = checkBoxXSD.Enabled;

            if (valTypeDTD)
                validator.Validate(Validator.ValTypes.DTD);
            if (valTypeXSD)
                validator.Validate(Validator.ValTypes.XSD);
        }

        private void ListView1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }
        private void ListView1_DragDrop(object sender, DragEventArgs e)
        {
            string[] fileList = (string[])e.Data.GetData(DataFormats.FileDrop);
            int fileListSize = fileList.Length;

            for (int i = 0; i < fileListSize; i++)
            {
                string ext = Path.GetExtension(fileList[i]);

                if (Props.IsInExtensionList(ext))
                {
                    AddFile(fileList[i]);
                }
            }
        }
        
        // << EVENTS

        public void AddFile(FileDetails fileDetails)
        {
            listView1.Items.Add(new ListViewItem(new string[] { fileDetails.path, "fine" }));
            FileDetails.allFiles.Add(fileDetails);
        }
        public void AddFile(string path)
        {
            FileDetails.allFiles.Add(new FileDetails(path));
            FileDetails test = FileDetails.allFiles.Last<FileDetails>();
            ListViewItem itemAdded = listView1.Items.Add(new ListViewItem(new string[] {test.id.ToString(), path, "" }));
        }
        public void RemoveFile(int id)
        {
            foreach(FileDetails fd in FileDetails.allFiles)
            {
                if (fd.id.Equals(id))
                {
                    FileDetails.allFiles.Remove(fd);
                    break;
                }
            }
            
            foreach (ListViewItem item in listView1.Items)
            {
                if (Int32.Parse(item.SubItems[index_id].Text) == id)
                {
                    listView1.Items.Remove(item);
                }
            }
            
        }
        


    }
}
