﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BMX_Validator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        [STAThread]
        static void Main()
        {
            FileDetails.allFiles = new List<FileDetails>();
            FileDetails.latestID = 0;

            // initialize extensions
            Props.EXTENSION_LIST = new List<string>();
            Props.EXTENSION_LIST.Add(".xml");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
